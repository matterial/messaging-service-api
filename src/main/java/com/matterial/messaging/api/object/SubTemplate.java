package com.matterial.messaging.api.object;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>SubTemplate</strong>
 */
@XmlRootElement
public class SubTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private Map<String, Object> contextObjects;

    public SubTemplate() {
        // *** do nothing;
    }

    public SubTemplate(String name) {
        this(name, null);
    }

    public SubTemplate(String name, Map<String, Object> contextObjects) {
        this();
        this.name = name;
        this.contextObjects = contextObjects;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getContextObjects() {
        if(this.contextObjects == null) {
            this.contextObjects = new HashMap<>();
        }
        return this.contextObjects;
    }

    public void setContextObjects(Map<String, Object> contextObjects) {
        this.contextObjects = contextObjects;
    }

    @Override
    public String toString() {
        return "SubTemplate{" +
               "name=" + this.getName() +
               ", contextObjects=" + this.getContextObjects() +
               "}";
    }

}
