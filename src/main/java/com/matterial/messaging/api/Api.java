package com.matterial.messaging.api;


/**
 * <strong>Api</strong> collection of all resource-paths and -params.
 */
public interface Api {

    public static final String APPLICATION_PATH = "api";

    // *** MODULE;
    public static final String MAIL = "mail";

}
